# Ansible Site Example

This is an example for an Ansible site with added GitLab CI features.

Also see [ansible-site-example-test](https://gitlab.com/lazyfrosch-org/ansible-site-example-test) for the usage in a separate repo for scheduling.

## License

Licensed under [Creative Commons Attribution 4.0 International License](http://creativecommons.org/licenses/by/4.0/).
